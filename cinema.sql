-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 23 2021 г., 20:27
-- Версия сервера: 8.0.24
-- Версия PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `cinema`
--

-- --------------------------------------------------------

--
-- Структура таблицы `moves`
--

CREATE TABLE `moves` (
  `id` int NOT NULL,
  `move` varchar(255) NOT NULL,
  `hero` varchar(255) NOT NULL,
  `produser` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Дамп данных таблицы `moves`
--

INSERT INTO `moves` (`id`, `move`, `hero`, `produser`) VALUES
(1, 'Капитан Крюк', 'Робин Уильямс', 'Стивен Спилберг'),
(2, 'Друг', 'Сергей Шакуров', 'Квинихидзе'),
(3, 'От заката до рассвета', 'Джорж Клуни', 'Квентин Тарантино'),
(4, 'Терминатор', 'Арнольд Шварцнеггер', 'Джеймс Кемерон');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `moves`
--
ALTER TABLE `moves`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `moves`
--
ALTER TABLE `moves`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
